#' segdata and gridata values diagnostic
#'
#' Make a boxplot of covariates of segdata and gridata to make sure they have
#' same order of magnitude
#'
#' @param segdata segdata data.frame
#' @param gridata gridata data.frame
#' @param covariates String of character. covariates to test.
#'
#' @return return of ggplot object
#' @export
#' @import ggplot2
#' @importFrom dplyr bind_rows select mutate
#' @importFrom tidyr pivot_longer
#'
#' @examples
seg_grid_diagnostic <- function(segdata, gridata, covariates) {

  theme_set(theme_bw())

  segdata_dignostic <- segdata %>% select(all_of(covariates)) %>% mutate(data = "segdata")
  gridata_diagnostic <- gridata %>% select(all_of(covariates)) %>% mutate(data = "gridata")

  df_diagnostic <- segdata_dignostic %>%
    bind_rows(gridata_diagnostic) %>%
    pivot_longer(cols = -data, names_to = "variable", values_to = "values")

  gg <- ggplot(df_diagnostic, aes(x = data, y = values, color = data)) +
    geom_boxplot() +
    facet_wrap(variable~., scales = "free_y")

  return(gg)

}
