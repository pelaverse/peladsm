library(stringr)
library(usethis)


dummy_dir <- tempfile(pattern = "dummy")
dir.create(dummy_dir)

withr::with_dir(dummy_dir, {
  test_that("Correct creation of script_by_sp for DSM", {
    tmp_dir <- tempdir()
    dir_proj <- file.path(tmp_dir,"R")
    create_project(tmp_dir)
    proj_set(tmp_dir)
    sp <- c("DELDEL","Petit delphinine")
    DSM_sp_files(sp)
    files_DSM <- list.files(file.path(dir_proj,"DSM/02_script_by_sp"), recursive = TRUE)
    files_fit <- files_DSM[str_detect(files_DSM,"fit_all_dsm")]

    expect_true(
      all(str_detect(files_fit,"(DELDEL|Petit delphinine)"))
    )

    # code_deldel <- readLines(file.path(dir_proj,"DSM/02_script_by_sp",files_DSM[str_detect(files_DSM,"DELDEL")])[1])
    # # print(code_deldel)
    # fit_deldel <- paste(
    #   code_deldel,
    #   collapse = "\n"
    # )
    # expect_true(
    #   str_detect(fit_deldel,'sp_name == "DELDEL"')
    # )

  })
})


