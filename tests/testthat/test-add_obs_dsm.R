

# Test only with MOLMOL (it's the same with JELLY...)

segdata_wo_obs <- SPEE_data_dsm$effort_output$segdata
legdata_wo_obs <- SPEE_data_dsm$effort_output$legdata

countdata_leg <- SPEE_data_dsm$list_prepare_obs_by_sp$MOLMOL_obs_output$countdata_leg
countdata_seg <- SPEE_data_dsm$list_prepare_obs_by_sp$MOLMOL_obs_output$countdata_seg


test_that("Correct behaviour of add_obs", {

  added_obs <- add_obs_dsm(
    segdata = segdata_wo_obs,
    countdata_seg = countdata_seg,
    legdata = legdata_wo_obs,
    countdata_leg = countdata_leg
  )

  # expected names of legdata
  expect_named(
    added_obs$legdata_obs,
    c(colnames(legdata_wo_obs),"n_detection","n_ind"),
    ignore.order = T
  )

  # expected names of legdata
  expect_named(
    added_obs$segdata_obs,
    c(colnames(segdata_wo_obs),"n_detection","n_ind"),
    ignore.order = T
  )

})
