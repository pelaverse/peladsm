<!-- NEWS.md is maintained by https://cynkra.github.io/fledge, do not edit -->

# pelaDSM 0.0.0.9006

- add `boat` argument in `cds_detection()`
- message in `fit_all_dsm()` when spline_by or spatial_options$by have empty factor
- warning message when by or spatial_options$by use factor with empty level(s)
- Fix issue on `fit_all_dsm()` with the soap


# pelaDSM 0.0.0.9005

* `fit_all_dsm()` gains a `spatial_options` parameter. It is a list which can contain `by`to adjust spatial spline according to varaiable and `complexity` to adjust k of spatial splines


# pelaDSM 0.0.0.9004

- New arugment in `prepare_soap()`, `bnd_limit` which allows to build soap with ocean limit or data limit


# pelaDSM 0.0.0.9003

* Add template to perform dsm analysis
* New `DSM_sp_files()` allows to iteratively create dsm repo by `sp_name` in the "DSM/script_by_sp" directory
* New `grid_to_poly()` to convert grid of points regularly distributed into a polygon grid


# pelaDSM 0.0.0.9002

* `complete_missing_cov()` now handles POINTS and POLYGON grid. If `lon` and `lat` are not in the grid they are built from geometry


# pelaDSM 0.0.0.9001

- Same as previous version.


# pelaDSM 0.0.0.9000

* Added a `NEWS.md` file to track changes to the package.
