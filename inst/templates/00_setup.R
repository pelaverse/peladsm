# clean environment
rm(list = ls())


#CRAN packages vector
cran_packages <- c(
  "devtools",
  "remotes",
  "tidyverse",
  "magrittr",
  "ggspatial",
  "sf",
  "cli",
  "crayon",
  "janitor",
  "stringr",
  "lubridate",
  "here",
  "units",
  "openxlsx",
  "purrr",
  "mgcViz"
)

github_packages <- c(
  NULL
)

github_repository <- c(
  NULL
)


# c_p_n_i : cran packages not installed
c_p_n_i <- cran_packages[!(cran_packages %in% installed.packages())]
# g_p_n_i : github packages not installed
gh_p_n_i <- which(!(github_packages %in% installed.packages()))


# installation of packages
lapply(c_p_n_i, install.packages, dependencies = TRUE)
lapply(github_repository[gh_p_n_i], devtools::install_github , dependencies = TRUE)

#install packages
lapply(c(cran_packages,github_packages), function(x){
  library(x, character.only = TRUE, quietly = TRUE)
})

rm(c_p_n_i, gh_p_n_i, cran_packages, github_packages, github_repository)


# add pelaDSM
if(!("pelaDSM" %in% installed.packages())){
  remotes::install_gitlab(
    repo = "pelaverse/peladsm",
    host = "gitlab.univ-lr.fr"
  )
}
library(pelaDSM)


