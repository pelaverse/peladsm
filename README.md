
<!-- README.md is generated from README.Rmd. Please edit that file -->

# pelaDSM

## Package installation

``` r
install.packages("remotes")
remotes::install_gitlab(
  host = "https://gitlab.univ-lr.fr",
  repo = "pelaverse/peladsm"
)
```
